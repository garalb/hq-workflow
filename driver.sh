#!/bin/sh

#
# This script generates "data" and submits an "analyzer" job every 2 seconds
#
# (make sure that the hq server has been started (hq server start)

FILESIZE="1MB"

while true
do
    sleep 2
    name=$(mktemp -u jobXXXXXX)
    dd if=/dev/zero of=${DATA_DIR}/$name.dat bs=${FILESIZE} count=1
    echo "Generating $name data and submitting job..."
    hq submit --name "Analysis" ./analyzer.sh "$name"
done

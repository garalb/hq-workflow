Simple framework for testing load-balancing
and other tricks in a producer-consumer workflow using HyperQueue.

The following will set up a simple run

```
export DATA_DIR=/some/path
hq server start  &
hq worker start --cpus 1 &
hq worker start --cpus 1 &
sh driver.sh >| LOG 2>&1 &
```

You can also replace the last line with 

```
hq worker start --cpus 1 &
hq submit --name "Driver" ./driver.sh & 
```

to make the driver part of HQ.  (Remember that one worker
would be exclusively in charge of running the driver)

